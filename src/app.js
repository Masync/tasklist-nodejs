//** Importando modulos nativos y de npm */
const path = require('path');
const express = require('express');
const morgan = require('morgan');

//** Init */
const app = express();

//**  Settings */
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//** Middleware */
app.use('/public', express.static('public'));
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));

//** Static files */
app.use(express.static(path.join(__dirname, 'public')));
//** Router */
app.use(require('./routes/entries.routes'));

//** 404 Handler */
app.use((req, res)=>{
    res.status(404).render('404page');

});

/** Start */
app.listen(app.get('port'), () =>{
    console.log('Server on port', app.get('port'));
});




