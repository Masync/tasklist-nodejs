const {Router} = require('express')
const route = Router();
const {renderIndex,renderNewEntry,renderCreateEntry} = require('../controllers/entries.controller')

route.get('/', renderIndex);
route.get('/new-task', renderNewEntry);
route.post('/new-Task', renderCreateEntry);

module.exports = route;